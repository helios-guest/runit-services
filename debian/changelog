runit-services (0.8.2) unstable; urgency=medium

  [ Andrew Bower ]
  * docker: ensure elogind starts first

  [ Lorenzo Puliti ]
  * all runscripts: drop starting message
  * fix purge/ piuparts failure
  * d/control: suggests: xchpst
  * update readme and package description
  * fix elogind for bookworm-trixie upgrade (Closes: #1095025)
  * postinst: clean obsolete metafiles on upgrade
  * NEWS file: elogind is force-updated
  * binpath-test: improve regex, mark as superficial
  * update copyright years

 -- Lorenzo Puliti <plorenzo@disroot.org>  Mon, 10 Feb 2025 15:31:00 +0100

runit-services (0.8.1) unstable; urgency=medium

  * improve binpath test:
    - fail only on wrong path, warning on missing binary path
  * update README (Closes: #1088001)

 -- Lorenzo Puliti <plorenzo@disroot.org>  Wed, 18 Dec 2024 11:19:38 +0100

runit-services (0.8.0) unstable; urgency=medium

  [ Lorenzo Puliti ]
  * d/control: update Vcs fields
  * bump Standards-Version to 4.7.0
  * update lintian overrides
  * create the sysv override directory in postinst,
     remove in postrm (purge)
  * fix purge logic
  * NEWS for runit-services users: dhclient issues
  * update d/copyright
  * autopkgtest: add a test for binary path (see #1069075)
  * all runfiles (source): use ##bin## placeholder
  * Drop services-test autopkgtest, it doesn't work with salsa
  * Anacron, apache2, cups, openntpd: add flagfiles to
     forward invoke-rc.d calls
  * elogind: force log to console and use svlogd
  * drop wicd, package no longer in Debian
  * override rsyslog logrotate file (Closes: #1079268)
  * add smartmontools runscript.
    Thanks to Dimitris T." <dimitris@stinpriza.org> (Closes: #1069188)
  * add unbound runscript.
    Thanks to Dimitris T." <dimitris@stinpriza.org> (Closes: #1083206)
  * add docker runscript

  [ Andrew Bower ]
  * gpm: fix command line overquoting preventing start (Closes: #1086143)
  * atd: fix daemon path, log to syslog
  * exim4: fix exim4 launch with trixie default env (Closes: #1087204)
  * add lldpd runscript
  * isc-dhcp-server: add runscript for IPv6 interfaces (Closes: #1086144)

 -- Lorenzo Puliti <plorenzo@disroot.org>  Thu, 21 Nov 2024 19:47:57 +0100

runit-services (0.7.2) unstable; urgency=medium

  * upload to unstable
  * Fix elogind service (Closes: #1069075)

 -- Lorenzo Puliti <plorenzo@disroot.org>  Mon, 15 Apr 2024 23:55:42 +0200

runit-services (0.7.1) experimental; urgency=medium

  * Fix piuparts failure in package removal
     (Closes: #1062682)
  * update years in copyright.in and
     regenerate d/copyright

 -- Lorenzo Puliti <plorenzo@disroot.org>  Fri, 02 Feb 2024 11:51:43 +0100

runit-services (0.7.0) experimental; urgency=medium

  [ Martin Steigerwald ]
  * new runscript for zcfan.

  [ Lorenzo Puliti ]
  * dhclient:
     - raise memory limit (Closes: #1035837)
     - remove the check file (Closes: #1038425)
     - dhclient: ship as disabled by default
  * new runscripts:
     - mpd, gpm
       + Thanks to Friedhelm Mehnert
     - nginx, lighttpd
  * Import the runscript from mini-httpd-run package:
    - change the runscript to use the default config file
    - d/control: runit-services Provides mini-httpd-run
  * d/copyright.in: add  GPL-3+ and GPL-2.0+ text
  * update d/copyright
  * update the testsuite:
    - test only a list of selected services
    - update the testsuite for new services layout
    - d/control: runit-services depends on runit(>=2.1.2-56)

 -- Lorenzo Puliti <plorenzo@disroot.org>  Sun, 15 Oct 2023 15:59:40 +0200

runit-services (0.6.0) experimental; urgency=medium

  * update-copyright script: allow "Comment:" line
  * new runscripts:
     avahi-daemon, avahi-dnsconfd, uuidd, bluetooth,
     network-manager, connman (Closes: #1032657)
  * dhclient: don't hardcode interfaces (Closes: #1033542)
  * update copyright
  * testsuite:
    - block all systemd services
    - fix pkgname detection with systemd service

 -- Lorenzo Puliti <plorenzo@disroot.org>  Mon, 27 Mar 2023 17:45:24 +0200

runit-services (0.5.4) unstable; urgency=medium

  * Add a flaky autopkgtest
    - d/tests/control file
    - blacklist services that are too hard or impossible
       to test
    - test all non-blacklisted services
  * update copyright
  * dbus.dep-fixer: wait for elogind only if it's enabled
  * isc-dhcp-server:
      - test config and lease
      - default to -4
      - configurable (empty by default) interfaces
  * dbus: simplify run script

 -- Lorenzo Puliti <plorenzo@disroot.org>  Thu, 19 Jan 2023 15:13:58 +0100

runit-services (0.5.3) unstable; urgency=medium

  * update Readme
  * runit-services: depends on runit >= 2.1.2-51
  * Bump Standards-Version
  * bump B-D dh-runit >= 2.15.2, to get rid of
     duplicates metafiles
  * release to unstable

 -- Lorenzo Puliti <plorenzo@disroot.org>  Thu, 29 Dec 2022 23:48:15 +0100

runit-services (0.5.2) experimental; urgency=medium

  * Improve purge documentation (Closes: #1024962)
  * Fix wrong boot order for sysv dbus-services (Closes: #1024969)
  * Automate d/copyright update for runscripts:
    - add a simple copyright header to each runfile
    - add d/copyright.in
    - add a script that automatically update d/copyright
      based on copyright.in and runscripts headers
  * update copyright and copyright.in
  * d/prerm: fix wrong path for meta files
  * All runscripts cleanup:
    - do not set -e, relevant commands are tested
    - do not use redundant 'sv check' for checking service
      dependencies, 'sv start' is enough

 -- Lorenzo Puliti <plorenzo@disroot.org>  Tue, 20 Dec 2022 01:50:14 +0100

runit-services (0.5.1) experimental; urgency=medium

  * dhclient:
     - raise memory limit
     - remove -e option
     - fix wrong keyword in check
     - move interfaces file in a conf subdirectory
  * update README
  * update copyright

 -- Lorenzo Puliti <plorenzo@disroot.org>  Sun, 20 Nov 2022 14:14:41 +0100

runit-services (0.5.0) experimental; urgency=medium

  * Reintroduce the package (closes: #986656)
  * new maintainer
  * Overall package update:
    - 3.0 native format, use dh sequencer
    - build with dh-runit >= 2.14.0 (closes: #359106)
    - dep5 copyright
    - bump S-V to 4.6.1
    - set Rules-requires-root to no
    - update package description
  * Enable the standard Salsa CI
  * Update/rewrite old services:
    - add apache2, remove apache;
    - add exim4, remove exim;
    - update cron, chrony, postfix, xdm,
      dhclient;
    - rename dhcp to isc-dhcpd-server
      (closes: #351051)
  * Add new services:
      acpi-fakekey, anacron, atd, cups, dbus, elogind,
      gdomap (gnustep), haveged, lightdm, lircd,
      mariadb, mdadm, openntpd, preload, proftpd,
      rsyslog, sddm, slim, wicd
  * Remove obsolete services:
     - ssh (already included in openssh-server package);
     - portmap (no longer in Debian)
     - nfs-kernel-server, rpc.statd, squid, exim,
       gdm (further work is needed)
  * Update copyright for new runscripts
  * Sync runscript at postinstall, by using 'cpsv --sync'
  * use runit triggers to enable/disable and restart services
  * Stop running services on package removal
  * postrm: disable services when the package is removed;
     remove service and log dirs when purge is requested
  * Update README
  * Add lintian override for empty log directories

 -- Lorenzo Puliti <plorenzo@disroot.org>  Sun, 23 Oct 2022 00:46:59 +0200

runit-services (0.4.0) unstable; urgency=low

  * sv/cron/run: no longer apply memory limit to cron (amd64 requires
    more than 11000000, thx Joost van Baal).
  * debian/rules: minor.
  * debian/control: Standards-Version: 3.8.0.1.

 -- Gerrit Pape <pape@smarden.org>  Tue, 17 Jun 2008 19:48:39 +0000

runit-services (0.3.2) unstable; urgency=low

  * debian/copyright: 2008.
  * debian/implicit: add proper dependencies to support 'parallel build'
    through make -j (thx Daniel Schepler for the patch).
  * README, sv/cron/run, sv/nfs-kernel-server/finish,
    debian/runit-services.prerm.in: use runit's update-service program to
    check/add/remove services, instead of dealing with symlinks in
    /var/service/ directly.
  * debian/control: Depends: runit (>= 1.8.0-2) (1st version that provides
    the update-service program).
  * debian/control: Standards-Version: 3.7.3.0.

 -- Gerrit Pape <pape@smarden.org>  Sun, 17 Feb 2008 17:10:12 +0000

runit-services (0.3.1) unstable; urgency=low

  * debian/control: move Recommends: dash, socklog-run to Suggests: (thx
    Daniel Kahn Gillmor, closes: #450808).

 -- Gerrit Pape <pape@smarden.org>  Mon, 10 Dec 2007 09:38:12 +0000

runit-services (0.3.0) unstable; urgency=low

  * debian/implicit: update to revision 1.11.
  * sv/ssh/run: make sure privilege separation directory /var/run/sshd
    exists (thx Daniel Kahn Gillmor for the patch, closes: #421398).
  * sv/rpc.statd/: new: service directory for rpc.statd (thx Daniel Kahn
    Gillmor for the patch, closes: #421401).

 -- Gerrit Pape <pape@smarden.org>  Sun, 06 Apr 2007 14:41:16 +0000

runit-services (0.2.3) unstable; urgency=low

  * sv/chrony: new; add chrony service directory.

 -- Gerrit Pape <pape@smarden.org>  Thu, 29 Jun 2006 08:23:18 +0000

runit-services (0.2.2) unstable; urgency=low

  * sv/portmap/run: use -f option to portmap, instead of -d.
  * debian/copyright: 2006.
  * debian/control: Standards-Version: 3.7.2.0.
  * debian/implicit: update to revision 1.11.

 -- Gerrit Pape <pape@smarden.org>  Sat, 27 May 2006 16:21:33 +0000

runit-services (0.2.1) unstable; urgency=low

  * sv/ssh/run: don't limit memory to ssh daemon by default (closes:
    #352959).
  * sv/dhcp/run: add $INTERFACES to arguments to dhcpd.

 -- Gerrit Pape <pape@smarden.org>  Sun, 26 Feb 2006 13:10:44 +0000

runit-services (0.2.0) unstable; urgency=low

  * sv/ssh/log/run: don't use "$_" in run script (patch from Joshua N
    Pritikin, closes: #350918).
  * debian/rules: create ./supervise/ subdirectory symlinks instead of
    including them in .tar.gz; symlink /var/log/<service>/ if exists to
    /etc/sv/<service>/log/main/.
  * sv/dhclient/log/run: don't use "$_" in run script.
  * debian/runit-services.prerm.in: new; shutdown enabled services on
    package removal.
  * debian/rules: create debian/runit-services.prerm from
    debian/runit-services.prerm.in.
  * debian/implicit: update to revision 1.11.

  * sv/portmap: new; add portmap service directory.
  * sv/nfs-kernel-server: new; add nfs-kernel-server services directory.
  * sv/dhcp: new; add dhcp service directory.

 -- Gerrit Pape <pape@smarden.org>  Mon,  6 Feb 2006 17:49:07 +0000

runit-services (0.1.0) unstable; urgency=low

  * initial version (closes: #330029).

 -- Gerrit Pape <pape@smarden.org>  Wed, 28 Dec 2005 10:33:01 +0000
